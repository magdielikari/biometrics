<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "person".
 *
 * @property string $id
 * @property string $name
 * @property string $ci
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $file_id
 *
 * @property Event[] $events
 * @property File $file
 * @property Record[] $records
 * @property Date[] $dates
 * @property Worked[] $workeds
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     *
     */
    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ci', 'file_id'], 'required'],
            [['ci', 'created_at', 'created_by', 'updated_at', 'updated_by', 'file_id'], 'integer'],
            [['name'], 'string', 'max' => 97],
            [['visible'], 'string', 'max' => 1], 
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'ci' => Yii::t('app', 'Ci'),
            'visible' => Yii::t('app', 'Visible'), 
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'file_id' => Yii::t('app', 'File ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecords()
    {
        return $this->hasMany(Record::className(), ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDates()
    {
        return $this->hasMany(Date::className(), ['id' => 'date_id'])->viaTable('record', ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkeds()
    {
        return $this->hasMany(Worked::className(), ['person_id' => 'id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getPermissions() 
    { 
        return $this->hasMany(Permission::className(), ['person_id' => 'id']); 
    } 
 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSummaries() 
    { 
        return $this->hasMany(Summary::className(), ['person_id' => 'id']); 
    } 
 
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getPeriods() 
    { 
        return $this->hasMany(Period::className(), ['id' => 'period_id'])->viaTable('summary', ['person_id' => 'id']); 
    } 

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getUsers() 
    { 
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('permission', ['person_id' => 'id']); 
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public static function username() 
    {
        $person = (new Query())
            ->select(['id', 'name', 'ci'])
            ->from('person')
            ->where(['visible' => 1])
            ->all();

        foreach ($person as $key => $value) {
            $name = $value['name'];
            $ci = $value['ci'];
            $ciName[] = [
                'id' => $value['id'],
                'ci' => $ci .' - ' . $name
            ];
        }
        return $ciName; 
    }

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getMembers() 
    { 
        return $this->hasMany(Member::className(), ['person_id' => 'id']); 
    } 
 
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getGroups() 
    { 
        return $this->hasMany(Group::className(), ['id' => 'group_id'])->viaTable('member', ['person_id' => 'id']); 
    } 

    /**
     * @inheritdoc
     * @return \app\models\query\PersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PersonQuery(get_called_class());
    }
}
