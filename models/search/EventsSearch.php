<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Event;
use app\models\Date;

/**
 * EventSearch represents the model behind the search form about `app\models\Event`.
 */
class EventsSearch extends Event
{
    public $global;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'number_years_day', 'unix_time', 'created_at', 'created_by', 'updated_at', 'updated_by', 'person_id'], 'integer'],
            [['event', 'global'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($person_id, $date_id)
    {
        //$query = Event::find();
        $date = Date::findOne(['id' => $date_id]);
        $number = $date->number_years_day;
        $year = $date->year;
        $query = Event::find()->year($year)->number($number)->person($person_id);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($person_id, $date_id);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
