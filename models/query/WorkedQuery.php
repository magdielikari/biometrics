<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Worked]].
 *
 * @see \app\models\Worked
 */
class WorkedQuery extends \yii\db\ActiveQuery
{
    /**
     *
     */
    public function person($person)
    {
        return $this->andWhere(['=', 'person_id', $person]);
    }

    /**
     *
     */
    public function date($date)
    {
        return $this->andWhere(['=', 'date_id', $date]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Worked[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Worked|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
