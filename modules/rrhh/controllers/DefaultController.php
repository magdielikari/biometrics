<?php

namespace app\modules\rrhh\controllers;

use Yii;
use app\models\Period;
use app\models\Record;
use app\models\Summary;
use app\models\search\RecordSearch;
use app\models\search\EventsSearch;
use app\models\search\WorkedsSearch;
use app\models\search\RecordsSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `report` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Record models.
     * @return mixed
     */
    public function actionIndex()
    {      
        $id = Yii::$app->user->getId();
        $searchModel = new RecordsSearch();
        $query = Yii::$app->request->queryParams;

        $years = Record::years();
        $months = Record::months();
        $people = Record::peoples($id);

        if ( empty($query) ) {
            $id = Record::id($id);
            $y = ArrayHelper::getColumn($years, 'year');
            $m = ArrayHelper::getColumn($months, 'month');
            $yn = count($y);
            $mn = count($m);
            $query = [
                'RecordsSearch' => [
                    'person' => $id,
                    'month' => $m[$mn-1],
                    'year' => $y[$yn-1] 
                ]
            ];
        }
        
        $year = ArrayHelper::getColumn($query, 'year');
        $month = ArrayHelper::getColumn($query, 'month');
        $person = ArrayHelper::getColumn($query, 'person');
        $per = Period::find()->year($year)->month($month)->one();
        $period = $per->id;

        $dataProvider = $searchModel->search($query);
        return $this->render('index', [
            'years' => $years,
            'people' => $people,
            'months' => $months,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findsModel($person, $period),
        ]);
    }

    /**
     * Displays a single Record model.
     * @param string $person_id
     * @param string $date_id
     * @return mixed
     */
    public function actionView($person_id, $date_id)
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search($person_id, $date_id);
        $searchsModel = new WorkedsSearch();
        $datasProvider = $searchsModel->search($person_id, $date_id);

        return $this->render('view', [
            'model' => $this->findModel($person_id, $date_id),
            'dataProvider' => $dataProvider,
            'datasProvider' => $datasProvider,
        ]);
    }

    /**
     * Finds the Record model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $person_id
     * @param string $date_id
     * @return Record the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($person_id, $date_id)
    {
        if (($model = Record::findOne(['person_id' => $person_id, 'date_id' => $date_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

        /**
     * Finds the Record model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $person_id
     * @param string $date_id
     * @return Record the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findsModel($person_id, $period_id)
    {
        if (($model = Summary::findOne(['person_id' => $person_id, 'period_id' => $period_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
