<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'ci',
            'visible',
            'created_at:datetime',
            [
                'attribute' => 'created_by',
                'value' => function($model){
                    return User::findIdentity($model->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updated_by',
                'value' => function($model){
                    return User::findIdentity($model->updated_by)->username;
                },
            ],
            [
                'attribute' => 'name',
                'value' => function($model){
                    return $model->file->name;
                },
            ]
        ],
    ]) ?>

</div>
