<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'People');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function($model){
            if($model->visible == 1){
                return ['class' => 'success'];
            }else{
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'ci',
            [
                'attribute' => 'created_at',
                'value' => function($dataProvider){
                    return Yii::$app->formatter->asDatetime($dataProvider->created_at, 'short');
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                }
            ],
            [
                'attribute' => 'name',
                'value' => 'file.name'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [  
                    'delete' => False
                    ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
