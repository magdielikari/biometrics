<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\search\RecordSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="record-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-4">
        <?= $form->field($model, 'person')->dropDownList(
            ArrayHelper::map($people, 'person', 'name'),
            ['prompt'=>'Seleccione un usuario']) 
        ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($model, 'month')->dropDownList(
            ArrayHelper::map($months, 'month', 'month'),
            ['prompt'=>'Seleccione un mes']) 
        ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'year')->dropDownList(
            ArrayHelper::map($years, 'year', 'year'),
            ['prompt'=>'Seleccione un año']) 
        ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
