<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use hscstudio\mimin\components\Mimin;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/log.png', ['alt'=>Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
        !Yii::$app->user->isGuest ? (
            ['label' => Yii::t('app', 'Upload'), 'items' => [
                    ['label' => Yii::t('app', 'File'),'url' => ['/upload/default']],
                    ['label' => Yii::t('app', 'Datas'),'url' => ['/upload/data']],
                    ['label' => Yii::t('app', 'Role'),'url' => ['/mimin/role']],
                    ['label' => Yii::t('app', 'Route'),'url' => ['/mimin/route']],
                    ['label' => Yii::t('app', 'Member'),'url' => ['/upload/member']],
                    ['label' => Yii::t('app', 'User'),'url' => ['/mimin/user']],
                    ['label' => Yii::t('app', 'Permissions'),'url' => ['/upload/permission']],
                ]
            ]
        ) : (            
            ['label' => Yii::t('app', 'Signup'), 'url' => ['/site/signup']]
        )
        ,!Yii::$app->user->isGuest ? (
            ['label' => Yii::t('app', 'RRHH'), 'items' => [
                    ['label' => Yii::t('app', 'Report'),'url' => ['/rrhh/default']],
                    ['label' => Yii::t('app', 'Person'),'url' => ['/rrhh/person']],
                    ['label' => Yii::t('app', 'Date'),'url' => ['/rrhh/date']],
                    ['label' => Yii::t('app', 'Group'),'url' => ['/rrhh/group']],
                    ['label' => Yii::t('app', 'Event'),'url' => ['/rrhh/event']],
                    ['label' => Yii::t('app', 'Period'),'url' => ['/rrhh/period']],
                    ['label' => Yii::t('app', 'Summary'),'url' => ['/rrhh/summary']],
                    ['label' => Yii::t('app', 'Worked'),'url' => ['/rrhh/worked']],
                ]
            ]
        ) : (            
            ['label' => Yii::t('app', 'Reset'), 'url' => ['/site/request-password-reset']]
        )
        ,!Yii::$app->user->isGuest ? (
            ['label' => Yii::t('app', 'Reporte'), 'items' => [
                    ['label' => Yii::t('app', 'Record'),'url' => ['/report/default']],
                ]
            ]
        ) : (            
            ['label' => Yii::t('app', 'Contact'), 'url' => ['/site/contact']]
        )
        ,!Yii::$app->user->isGuest ? (
            ['label' => Yii::t('app', 'Usuario'), 'items' => [
                    ['label' => Yii::t('app', 'Record'),'url' => ['/user/default']],
                ]
            ]
        ) : (            
            ['label' => Yii::t('app', 'Contact'), 'url' => ['/site/contact']]
        )
        ,Yii::$app->user->isGuest ? (
            ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::t('app', 'Logout') . '(' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )
    ];
    $menuItems = Mimin::filterMenu($menuItems);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,   
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Premiere Soluciones Integrales C.A. <?= date('Y') ?></p>
        
        <p class="pull-right">&emsp; Colaboradores: Marifé Reyes, Omar Gonzalez, Xiomara Salcedo y Luis Arcia.</p>
        <p class="pull-right">Desarrollado por el Lic. Magdiel Márquez</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
